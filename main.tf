resource "aws_s3_bucket" "sir_buck_buck" {
  count         = var.bucket_num
  bucket        = "${var.bucket_name_prefix}-${count.index}"
  force_destroy = var.force_destroy
  tags = {
    Name        = var.bucket_num
    Environment = "Dev"
  }
}
