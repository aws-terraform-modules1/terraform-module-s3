variable "bucket_num" {
  type        = number
  description = "(int, Required)"
}

variable "force_destroy" {
  type        = bool
  description = "(bool, Optional) decides whether to allow terraform to delete the bucket if it has items (true = yes delete it anyways)"
  default     = true
}

variable "bucket_name_prefix" {
  type        = string
  description = "(string, Optional) prefix name of the bucket"
  default     = "sir-buck-buck-the"
}